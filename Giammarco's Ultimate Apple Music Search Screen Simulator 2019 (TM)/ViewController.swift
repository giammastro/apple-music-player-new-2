//
//  ViewController.swift
//  Giammarco's Ultimate Apple Music Search Screen Simulator 2019 (TM)
//
//  Created by Giammarco Mastronardi on 11/12/2019.
//  Copyright © 2019 Giammarco Mastronardi. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        print("Search")
    }

    @IBOutlet weak var tableView: UITableView!
    
    var songs: [Song] = []
    var resultSearchController: UISearchController?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        songs = createArray()
       
        tableView.delegate = self
        tableView.dataSource = self
        
        self.resultSearchController = ({
            // creo un oggetto di tipo UISearchController
            let controller = UISearchController(searchResultsController: nil)
            // rimuove la tableView di sottofondo in modo da poter successivamente visualizzare gli elementi cercati
            controller.dimsBackgroundDuringPresentation = false

            // il searchResultsUpdater, ovvero colui che gestirà gli eventi di ricerca, sarà la ListaTableViewController (o self)
            controller.searchResultsUpdater = self as? UISearchResultsUpdating

            // impongo alla searchBar, contenuta all'interno del controller, di adattarsi alle dimensioni dell'applicazioni
            controller.searchBar.sizeToFit()
            
            // atacco alla parte superiore della TableView la searchBar
            self.tableView.tableHeaderView = controller.searchBar
            
            // restituisco il controller creato
            return controller
        })()
    }
    
    func createArray() -> [Song] {
        
        var tempSongs: [Song] = []
        
        let song1 = Song(image: UIImage(named: "miele")!, title: "Miele", artist: "Gigi D'Alessio")
        let song2 = Song(image: UIImage(named: "dontstartnow")!, title: "Don't Start Now", artist: "Dua Lipa")
        let song3 = Song(image: UIImage(named: "blacksands")!, title: "Black Sands", artist: "Bonobo")
        
        tempSongs.append(song1)
        tempSongs.append(song2)
        tempSongs.append(song3)
        
        return tempSongs
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let song = songs[indexPath.row]
        let cell = tableView .dequeueReusableCell(withIdentifier: "SongCell") as! SongCell
    
    cell.setSong(song: song)
    
        return cell
        edtrf,kjhgvfcdxcfvbcdxnjlmkjhgfytdfvgbnjlmkj
    }
}

